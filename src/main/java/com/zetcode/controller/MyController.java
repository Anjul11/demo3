package com.zetcode.controller;

import com.zetcode.bean.CabEntity;
import com.zetcode.bean.User;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Controller
public class MyController {

  
//     @Autowired
//      TelemetryClient telemetryClient;

     @GetMapping("/addUser")
     public String sendForm(User user) {
         //track a custom event  
        //  telemetryClient.trackEvent("Sending a custom event...");
 
        //  //trace a custom trace
        //  telemetryClient.trackTrace("Sending a custom trace....");
          return "addUser";
     }
    
    @GetMapping("/getAllCabs")
    public String sendForm(Model model, User user) {

        String CAB_HOST="https://springbackend-api.azurewebsites.net/";

        RestTemplate restTemplate=new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        String url = CAB_HOST + "cabs/getAvailableCabs";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("areaCode", "011");


        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<List<CabEntity>> exchange = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<CabEntity>>() {
                });

        List<CabEntity> cabEntities = exchange.getBody();
        System.out.println(cabEntities.get(0).getAreaCode() + ":   "+cabEntities.get(0).getDriverName());
        user.setCabEntities(cabEntities);
        model.addAttribute("allCabs",cabEntities);
        model.addAttribute("user",user);
      //Print these entities to check if values are coming
        //track a custom event  
        // telemetryClient.trackEvent("Sending a custom event...");


        // //trace a custom trace
        // telemetryClient.trackTrace("Sending a custom trace....");
        return "showCabs";
    }


    @PostMapping("/bookCab")
    public String processForm(Model model, HttpServletRequest httpServletRequest) {

        String cabId = httpServletRequest.getParameterValues("cabEntities")[0];
    //change this host
            String CAB_HOST="https://springdemo1-api.azurewebsites.net/";
            
            RestTemplate restTemplate=new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        String url = CAB_HOST + "bookcab/bookAcab";

        //change cabId as per your db
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("cabId", cabId);


        HttpEntity<?> entity = new HttpEntity<>(headers);
        // telemetryClient.trackEvent("Sending a custom event...");
 
        //  //trace a custom trace
        //  telemetryClient.trackTrace("Sending a custom trace....");

        ResponseEntity<String> exchange = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<String>() {
                });
String str= exchange.getBody();

//check this value in server to see if second api is called successfully.
        System.out.println("str");
                

        return "cabBooked";
    }
    
   
}
